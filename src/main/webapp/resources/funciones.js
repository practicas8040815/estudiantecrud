$(function () {
    $(".modalDisplay").click(function () {
        var mod = $(this).attr('data-target');
        $(".mdNuevo").find("input,textarea,select").val("");
        $("#" + mod).modal("show");
        $("#cod").attr('readonly', false);
    });
    $(".closeDlg").click(function () {
        $(".modal").modal("hide");
    });
    $(".editar").click(
        function () {
            var cod = $(this).attr('data-target');
            console.log("entró al editar codigo=" + cod);
            $.getJSON('/EstudianteCRUD/formularioEditarEstudiante',
                "codigo=" + cod,
                function (data) {
                    $('.mdNuevo').modal("show");
                    $("#cod").val(data["codigo"]);
                    $("#nombre").val(data["nombre"]);
                    $("#apellido").val(data["apellido"]);
                    $("#cum").val(data["cum"]);
                    $("#cuotaMensual").val(data["cuotaMensual"]);
                    $("#tituloGuardar").val(data["Modificar Estudiante"]);
                    $("#cod").attr('readonly', true);
                }
            )
        });

    $('#nuevoEstudiante').click(function () {
        $('.mdNuevo').modal("show");
        $("#cod").val("");
        $("#nombre").val("");
        $("#apellido").val("");
        $("#cum").val("");
        $("#cuotaMensual").val("");
        $("#tituloGuardar").text("Nuevo Estudiante");

    });
    $('#MnEstudiante').click(function(){
        $('.estudiante').show();
    });
})