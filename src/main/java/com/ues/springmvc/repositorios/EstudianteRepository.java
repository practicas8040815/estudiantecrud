package com.ues.springmvc.repositorios;

import com.ues.springmvc.entidades.Estudiante;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class EstudianteRepository {
    @PersistenceContext
    private EntityManager em;
    public List<Estudiante> buscarTodos(){
        TypedQuery<Estudiante> consultaJPA = em.createQuery(
                "select f from Estudiante f", Estudiante.class
        );
        return consultaJPA.getResultList();
    }
    public List<Estudiante> buscarByNombre(String nombre){
        TypedQuery<Estudiante> consultaJPA = em.createQuery(
                "select f from Estudiante f where f.nombre like '%"+nombre+"%'", Estudiante.class
        );
        return consultaJPA.getResultList();
    }
    @Transactional
    public void insertar(Estudiante estudiante){
        em.persist(estudiante);
    }
    @Transactional
    public void borrar(Estudiante estudinte){
        em.remove(em.merge(estudinte));
    }
    @Transactional
    public void actualizar(Estudiante estudiante){
        em.merge(estudiante);
    }
    @Transactional
    public Estudiante buscarUnEstudiante(int codigo){
        return em.find(Estudiante.class, codigo);
    }
}
