package com.ues.springmvc.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity(name="Estudiante")
@Table(name="estudiante")
public class Estudiante implements Serializable {
    @Id
    @Column(name="codigo")
    private Integer codigo;
    @Column(name="nombre")
    private String nombre;
    @Column(name="apellido")
    private String apellido;
    @Column(name="cum")
    private Double cum;
    @Column(name="cuota")
    private Double cuotaMensual;

    public Estudiante() {
    }

    public Estudiante(Integer codigo) {
        this.codigo = codigo;
    }

    public Estudiante(Integer codigo, String nombre, String apellido, Double cum, Double cuotaMensual) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cum = cum;
        this.cuotaMensual = cuotaMensual;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Double getCum() {
        return cum;
    }

    public void setCum(Double cum) {
        this.cum = cum;
    }

    public Double getCuotaMensual() {
        return cuotaMensual;
    }

    public void setCuotaMensual(Double cuotaMensual) {
        this.cuotaMensual = cuotaMensual;
    }
}
