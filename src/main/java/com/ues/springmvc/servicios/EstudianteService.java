package com.ues.springmvc.servicios;

import com.ues.springmvc.entidades.Estudiante;
import com.ues.springmvc.repositorios.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EstudianteService {
    @Autowired
    EstudianteRepository repositorio;

    @Transactional
    public void insertarVarios(Estudiante e1, Estudiante e2){
        repositorio.insertar(e1);
        repositorio.insertar(e2);
    }
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Estudiante> buscarTodos(){
        return repositorio.buscarTodos();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void insertar(Estudiante estudiante){
        repositorio.insertar(estudiante);
    }
    @Transactional
    public void borrar(Estudiante estudiante){
        repositorio.borrar(estudiante);
    }
    @Transactional
    public void actualizar(Estudiante estudiante){
        repositorio.actualizar(estudiante);
    }
    @Transactional
    public Estudiante buscarUnEstudiante(int codigo){
        return repositorio.buscarUnEstudiante(codigo);
    }
    @Transactional
    public List<Estudiante> buscarByNombre(String nombre){
        return repositorio.buscarByNombre(nombre);
    }
}
