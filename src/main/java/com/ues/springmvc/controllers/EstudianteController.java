package com.ues.springmvc.controllers;

import com.ues.springmvc.entidades.Estudiante;
import com.ues.springmvc.servicios.EstudianteService;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
public class EstudianteController {
    @Autowired
    EstudianteService servicio;

    @RequestMapping("/")
    public String menuApp(Model modelo){
        listarEstudiante(modelo);
        return "listaEstudiantes";
    }
    @RequestMapping("/listaEstudiantes")
    public String listarEstudiante(Model modelo){
        List<Estudiante> lista = servicio.buscarTodos();
        modelo.addAttribute("estudiante", new Estudiante());
        modelo.addAttribute("listaEstudiantes",lista);
        return "listaEstudiantes";
    }
    @RequestMapping("/formularioEditarEstudiante")
    public @ResponseBody String formularioEditarEstudiante(@RequestParam("codigo") int codigo, Model modelo) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        String resp = "";
        Estudiante estudiante = servicio.buscarUnEstudiante(codigo);
        try{
            resp = mapper.writeValueAsString(estudiante);
            System.out.println(resp);
        }catch (JsonProcessingException e){

        }
        return  resp;
    }

     //permite borrar un estudiante
    @RequestMapping("/borrarEstudiante")
    public String borrarEstudiante(@RequestParam("codigo")int codigo, Model modelo){
        servicio.borrar(new Estudiante(codigo));
        return listarEstudiante(modelo);
    }
        @RequestMapping("/formularioGuardarEstudiante")
    public String formularioEditarEstudiante(Model modelo, @ModelAttribute Estudiante estudiante){
        servicio.actualizar(estudiante);
        return  listarEstudiante(modelo);
    }
}
